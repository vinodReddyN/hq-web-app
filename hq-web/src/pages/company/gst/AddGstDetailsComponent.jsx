import React, { Component } from "react";
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import GstService from "../../../services/gst/GstService.js";
import loader from '../../../assets/images/loader.gif';
const gstCodesMasterData = require("../../../assets/json/GSTCodesMasterData.json");

class AddGstDetailsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gstNumber: "",
      gstHolderName: "",
      gstHolderaddress: "",
      gstStateCode: "",
      gstStateName: "",
      message: "",
      loading:''
    };
    this.handleChange = this.handleChange.bind(this);
    this.backBtnEvent = this.backBtnEvent.bind(this);
    this.addgstDetEvent = this.addgstDetEvent.bind(this);
    //alert('this.props.gstCompanyName '+this.props.gstCompanyName);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value,message:'' });
  };

  addgstDetEvent(e) {
    this.state.loading='true';
    this.forceUpdate();

    if(this.state.gstNumber!==undefined && this.state.gstNumber !==''){
    this.state.gstStateCode = this.state.gstNumber.slice(0, 2);
    if (
      this.state.gstNumber !== undefined &&
      this.state.gstNumber !== "" &&
      this.state.gstNumber.slice(0, 2) != undefined &&
      this.state.gstNumber.slice(0, 2) !== ""
    ) {
      this.state.gstStateName =
        gstCodesMasterData[this.state.gstNumber.slice(0, 2)].state_name;
      this.state.gstStateCode = this.state.gstNumber.slice(0, 2);
    }

    const gsteditDetails = [
      {
        gst_holder_address: this.state.gstHolderaddress,
        gst_holder_name: this.state.gstHolderName,
        gst_holder_state_code: this.state.gstStateCode,
        gst_holder_state_name: this.state.gstStateName,
        gst_number: this.state.gstNumber
      }
    ];

    this.addGstReq = {
      domain_name: this.props.gstCompanyName,
      gst_details: gsteditDetails
    };

    this.addGstDetailsReq(this.addGstReq);    
  }else{
    this.state.loading='false';
    this.state.message = 'GST number must be required';
    this.forceUpdate();
  }
}

  addGstDetailsReq(addGstReq) {
    this.state.message = "";
    GstService.addGstCompanyDetails(addGstReq).then(response => {
      const addGstResponse = response.data;
      this.setState({ addGstResponse });
      console.log(
        "addGstCompanyDetails Res " + JSON.stringify(this.state.addGstResponse)
      );

      if (
        this.state.addGstResponse !== undefined &&
        this.state.addGstResponse !== "" &&
        this.state.addGstResponse.status !== undefined &&
        this.state.addGstResponse.status !== "" &&
        this.state.addGstResponse.status === 200
      ) {
        const msg='GST number ('+this.state.gstNumber+ ') added successfully'
        //alert(msg);
        this.setState({ message: msg });        
        this.props.gstDetailsMsg(this.state.message);
      }else{
        this.state.message = 'GST number not added  ';
        this.state.loading='false';
        this.forceUpdate();
        

      }
    });
  }

  backBtnEvent(e) {
    const backbtn = "SEARCH PAGE";
    this.props.gstAddDetailsHandler(backbtn);
  }

  render() {
    return (
      <>
        <Header />
        <div className="MainContainer pageTtl">
          <h3>Add GST Record</h3>
        </div>
        <div className="MainContainer cont-Box">
          {(this.state.message !== "" && this.state.message !== undefined) &&(
            <div className="alert alert-warning">
              {this.state.message}
            </div>
          )}
          <div className="selectedComp">
          <label className="SubTitle">Domain : <strong>{this.props.gstCompanyName}</strong></label></div>
          <div className="form-group">
            <div className="row">
              <div className="col-2">
                <label>GST number <span className="required">*</span> :</label>
              </div>
              <div className="col-3">
                <input
                  type="text"
                  name="gstNumber" placeholder='Enter GST number'
                  onChange={this.handleChange}
                  value={this.state.gstNumber}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-2">
                <label>GST holder name :</label>
              </div>
              <div className="col-3">
                <input
                  type="text" placeholder='Enter GST holder name'
                  name="gstHolderName"
                  onChange={this.handleChange}
                  value={this.state.gstHolderName}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-2">
                <label>GST holder address :</label>
              </div>
              <div className="col-3">
                <input
                  type="text" placeholder='Enter GST holder address'
                  name="gstHolderaddress"
                  onChange={this.handleChange}
                  value={this.state.gstHolderaddress}
                />
              </div>
            </div>
          </div>

          <div className="btnSec">
            <button
              type="button"
              onClick={this.backBtnEvent}
              className="btn btn-xs btn-default"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={this.addgstDetEvent}
              className="btn btn-xs btn-primary"
            >
              Add Gst
            </button>
            <span className="ldrPart">{(this.state.loading==='true') && <img src={loader} className="App-logo" alt="loader" />}</span>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}
export default AddGstDetailsComponent;
