import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import loader from '../../../assets/images/loader.gif';

class CompanyDetailsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            arrayData:this.props.detailsPagedata,
            loading:'' 
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e){
        this.state.loading='true';
        this.props.clickCompanyDetailsHandler(this.state.arrayData);
        this.state.loading='false';
    }
    gotoSearchPage = () => {
        this.props.clickEditBackAddHandler('searchPage');
    }

    render() {
        return (
            <>
            <Header />
            <div className="MainContainer pageTtl">
            <h3> Company Config Details <span className="titleBtn"><button type="button" onClick={this.gotoSearchPage} className="btn btn-xs btn-default"><b>Back</b></button><button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary">edit company config</button></span></h3>
                </div>
            <div className="MainContainer cont-Box">
            
            <table className="configtable dataTbl" id="contentDataId" key="dataItm">
                <tbody>
                <tr><th>Config Name</th><th>Config Value</th></tr>
                {this.state.arrayData.company_configs.length > 0 &&  this.state.arrayData.company_configs.map((person, index) => (
                    <tr key={index}>
                         <td width="35%">{person.config_name}</td>
                         <td width="65%"><span title={person.config_value}>{person.config_value}</span></td>
                            </tr>
                        ))}
                </tbody>
                </table>
                <div className="btnSec">
                <button type="button" onClick={this.gotoSearchPage} className="btn btn-xs btn-default"><b>Back</b></button><button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary"><b>edit company config</b></button>
                <span className="ldrPart">{this.state.loading==='true' && <img src={loader} className="App-logo" alt="loader" />}</span>
                </div>
            </div>

            <Footer />
            </>
        )
    }
}

export default CompanyDetailsComponent
