import React, { Component } from 'react';
import "typeface-roboto";
//import './App.css';
import './assets/css/main.css'
import './assets/css/gridStyle.css'
import './assets/json/selection.json';
import InstructorApp from './router/InstructorApp.jsx';

class App extends Component {
  render() {
    return (
      
        <InstructorApp />

    );
  }
}

export default App;