import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class FlightTripDtls extends Component {

    constructor(props) {
        super(props)
        this.state = {
            
            tripDtls :[]
          
        }
      this.showNewPopUp=this.showNewPopUp.bind(this);
    }
    
    showNewPopUp(){
       window.open(
            "https://qa2.cleartrip.com/hq/trips/73ea35e4fe-6c7e-4a33-9691-191018233426/get_int_fare_rule",
            "Fare Rules",
            "resizable,scrollbars,status"
          );
    }
    
    render() {
       return(
           
       this.props.flt.map(
          ele=>{
            
              return(
             
                
                ele.type_detail ==="Flight"
              ?    
              <div>{this.props.flt.indexOf(ele)===0?
              <span><h4>Itinerary</h4>
              <Link id="fareRule" onClick= {this.showNewPopUp}>Fare Rule</Link></span>:null}
              <table>
                  <thead>
                 
         <div>{ele.departure_airport} to {ele.arrival_airport}</div>
        
         <tr><th >
             </th></tr>
             <th>Leave</th><th>Arrive</th><th>Airlines</th><th>Duration</th><th>Flight</th><th>Stops</th>
             <th>Class</th><th>Supplier</th>
             </thead>
             <tbody>
                 { 
                    ele.segments.map(seg=><tr key={seg.id}>
                     <td>{seg.departure_airport} {seg.departure_terminal}</td>
                     <td>{seg.arrival_airport} {seg.arrival_terminal}</td>
                     <td>{seg.operating_airline}</td>
                     <td></td>
                     <td>{seg.operating_airline} - {seg.flight_number}</td>
                     <td>{seg.stopover_count}</td>
                     <td></td>
                     <td>{seg.supplier}</td>
                    </tr>

                 )
                 }
                 
             </tbody>
                 
              </table> </div>:<table>
                  <thead>
         <span><h4>Flight booking details</h4>          
         <div>{ele.title} {ele.first_name}  {ele.last_name}</div>
         </span>
         <tr><th >
             </th></tr>
             <th>Sector</th>
                    <th>Status</th>
                    <th>Airline Pnr</th>
                    <th>Split Pnr</th>
                    <th>Gds Pnr</th>
                    <th>Fare Class</th>
                    <th>Fare basic Code</th>
                    <th>BF + Airline Penalty</th>
                    <th>Ticket</th>
                    <th>Booking Type</th>
                    <th>Cost Pricing</th>
             </thead>
             <tbody>
                 { 
                    ele.paxToFlightMap.map(info=><tr key={info.id}>
                     <td>{info.departure_airport} - {info.arrival_airport}</td>
                    <td>{info.booking_status}</td>
                    <td>{info.airline_pnr}</td>
                    <td></td>
                    <td>{info.gds_pnr}</td>
                    <td>{info.cabin_type}</td>
                    <td>{info.pricing_objects[0].fare_basis_code}</td>
                    <td>{info.pricing_objects[0].total_base_fare}</td>
                    <td>{info.ticket_number}</td>
                    <td></td>
                    <td>{info.pricing_objects[0].cost_pricing_object.total_fare}</td>
                    </tr>

                 )
                 }
                 
             </tbody>
                 
              </table> )
              
          }
       )

       )
       
         
}

}


export default FlightTripDtls;