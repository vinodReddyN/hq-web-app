import React, { Component } from 'react'


class HotelTripDtls extends Component {

    constructor(props) {
        super(props)
        this.state = {
            content :''
          
        }
        
    }

    
    
    render() {
        
        return (
           
            this.props.htl.map(
               ele=>{
                 
                   return(
                  
                     
                     ele.type ==="hotel_booking"
                   ?    
                   <div>
                   <table>
                       <thead>
                      
              <div>Hotel in {ele.city_name} - {ele.hotel_name}</div>
              <tr><th >
                  </th></tr>
                  <th>Check in</th><th>Check out</th><th>Duration</th><th>Rooms</th>
                  
                  </thead>
                  <tbody>
                      { 
                         <tr key={ele.id}>
                          <td>{ele.check_in_date}</td>
                          <td>{ele.check_out_date}</td>
                          <td></td>
                          <td>{ele.room_count} {ele.room_type}</td>
                         </tr>
     
                    
                      }
                      
                  </tbody>
                      
                   </table> </div>:
                   <table>
                       <thead>
                           
              <div><h4>Hotel booking details : {ele.no_of_adult} Adults</h4></div>
              <tr><th >
                  </th></tr>
                  <th>Room Type</th>
                         <th>Supplier</th>
                         <th>Voucher</th>
                         <th>Number of Travellers</th>
                         <th>Status</th>
                         
                  </thead>
                  <tbody>
                  { ele.room_info.map(info=><tr key={info.id}>
                        <td>{info.room_name} </td>
                        <td>{info.supplier_name}</td>
                        <td>{info.voucher_number}</td>
                        <td></td>
                        <td>{info.status}</td>
                        
                       </tr>
                      )}
                      
                  </tbody>
                      
                   </table> )
                   
               }
            )
     
            )
    }


}

export default HotelTripDtls;