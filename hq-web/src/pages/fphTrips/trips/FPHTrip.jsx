import React, { Component } from 'react'
import FlightTripDtls from './FlightTripDtls'
import HotelTripDtls from './HotelTripDtls'

class FPHTrip extends Component {

    constructor(props) {
        super(props)
        this.state = {
            content:""
        }
        
    }

    
    
    render() {
        
        return (
           
            <>
            <div>
                
            <div><h1>Flight Itinerary</h1></div>
                <FlightTripDtls flt={this.props.fltDtl}/>
                 <div><h1>Hotel Itinerary</h1></div>
                <HotelTripDtls htl={this.props.htlDtl}/>
                <div>
                   <h4>Inclusion And Extra</h4>
                   <span>{this.props.htlDtl[1].inclusion}</span>

                </div>
            </div>
           
</>

        )
    }


}

export default FPHTrip;