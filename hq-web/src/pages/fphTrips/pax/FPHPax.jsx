import React, { Component } from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
const fltRS = require('../../../assets/json/data.json');
const pax = fltRS.air_bookings[0].pax_infos;
//const airMasterData = require('../../../assets/json/AirlinesMasterData.json');

class FPHPax extends Component {

  constructor(props) {
    super(props)
    let paxInfos = [];
    this.state = {
      fphRS: fltRS,
      paxInfos: pax,
      showEditPax: false,
      selectedDay: undefined,
      mealMap : [
                {code : "AVML", name : "Asian Veg. Meal"},
                {code : "BBML", name : "Inf/Baby Food"},
                {code : "BLML", name : "Bland Meal"},
                {code : "CHML", name : "Child Meal"},
                {code : "DBML", name : "Diabetic Meal"}
              ],
              airlinesMaster : []
    }
    this.handleEditPaxDetail = this.handleEditPaxDetail.bind(this);
    this.savePaxDetails = this.savePaxDetails.bind(this);

  }

  handleEditPaxDetail(e) {
    this.setState(state => ({
      showEditPax: !state.showEditPax
    }));
  }

  componentDidMount() {
    fetch('http://staging.flyinstatic.com/hq/json/AirlinesMasterData.json')
      .then(response => response.json())
      .then(airlinesMaster => this.setState({ airlinesMaster }));
  }
  savePaxDetails(e) {
    let air_bkng = this.state.fphRS.air_bookings;
    for (let airBkng of air_bkng) {
      airBkng.pax_infos = this.state.paxInfos;
    }
    // console.log("fphRS Updated : "+JSON.stringify(this.state.fphRS))
  }

  handleChange = idx => evt => {
    let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
      if (idx !== sidx) return paxInfo;
      return {
        ...paxInfo, person_id: evt.target.value
      };
    });
    this.setState({ paxInfos: newPaxinfo });
  }
  handleChangeCountry = idx => evt => {
    let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
      if (idx !== sidx) return paxInfo;
      return {
        ...paxInfo, birth_country: evt.target.value
      };
    });
    this.setState({ paxInfos: newPaxinfo });
  }

  handleChangeNation = idx => evt => {
    let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
      if (idx !== sidx) return paxInfo;
      return {
        ...paxInfo, pax_nationality: evt.target.value
      };
    });
    this.setState({ paxInfos: newPaxinfo });
  }

  handleChangeVisaType = idx => evt => {
    let newPaxinfo = this.state.paxInfos.map((paxInfo, sidx) => {
      if (idx !== sidx) return paxInfo;
      return {
        ...paxInfo, birth_country: evt.target.value
      };
    });
    this.setState({ paxInfos: newPaxinfo });
  }

  handleDayChange = selectedDay => {
    this.setState({ selectedDay });
  };

  render() {
    // const value = this.state.selectedDay
    //   ? this.state.selectedDay.format("DD/MM/YYYY")
    //   : "";


    let meals = this.state.mealMap;
        let mealsMenu = meals.map((meal) =>
                <option value={meal.code}>{meal.name}</option>
            );
       let airMaster = this.state.airlinesMaster;
        let airlines = airMaster.map((airline) =>
                <option value={airline.airCode}>{airline.airName}</option>
            );

    return (
      <div>
        <div>
          {this.state.paxInfos.map((paxInfo, index) => (
            <div key={index}><h3>{paxInfo.title} {paxInfo.first_name}  {paxInfo.last_name} <small>{paxInfo.pax_type_code}</small></h3>
              <table className="dataTbl pax-tbl">
                <thead><tr>
                  <th>Date Of Birth</th>
                  <th>Passport#</th>
                  <th>Issuing country</th>
                  <th>Nationality</th>
                  <th>Expires on</th>
                  <th>Meal code</th>
                  <th>Visa type</th>
                  {this.state.showEditPax === false && (
                    <th>Baggage code</th>
                  )}
                </tr></thead>
                <tbody>
                  <tr>
                    <td>
                      {this.state.showEditPax === true && (
                        <DayPickerInput
                          format="DD/MM/YYYY"
                          value={paxInfo.date_of_birth}
                          onDayChange={this.handleDayChange}
                        />
                      )}
                      {this.state.showEditPax === true && (
                        <span>{paxInfo.date_of_birth}</span>
                      )}
                    </td>
                    <td>
                      {this.state.showEditPax === true && (
                        <input type="text" name="paxInfo.person_id" value={paxInfo.person_id} onChange={this.handleChange(index)} />
                      )}
                      {this.state.showEditPax === false && (
                        <span>{paxInfo.person_id}</span>
                      )}
                    </td>
                    <td>
                      {this.state.showEditPax === true && (
                        <input type="text" name="paxInfo.birth_country{index}" value={paxInfo.birth_country} onChange={this.handleChangeCountry(index)} />
                      )}
                      {this.state.showEditPax === false && (
                        <span>{paxInfo.birth_country}</span>
                      )}
                    </td>
                    <td>
                      {this.state.showEditPax === true && (
                        <input type="text" name="paxInfo.pax_nationality" value={paxInfo.pax_nationality} onChange={this.handleChangeNation(index)} />
                      )}
                      {this.state.showEditPax === false && (
                        <span>{paxInfo.pax_nationality} </span>
                      )}
                    </td>
                    <td>
                      {this.state.showEditPax === true && (
                        <DayPickerInput
                          format="DD/MM/YYYY"
                          value={paxInfo.created_at}
                          onDayChange={this.handleDayChange}
                        />
                      )}
                      {this.state.showEditPax === false && (
                        <span>{paxInfo.created_at}</span>
                      )}
                    </td>
                    <td> {this.state.showEditPax === true && (<select name="paxInfo.meal_request_code" value={paxInfo.meal_request_code} onChange={this.handleChange}>
                      {mealsMenu}
                    </select>)}</td>
                    {this.state.showEditPax === false && (
                      <span>{paxInfo.meal_request_code}</span>
                    )}
                    <td>
                      {this.state.showEditPax === true && (
                        <input type="text" name="visaType" onChange={this.handleChangeVisaType(index)} />
                      )}
                      {this.state.showEditPax === false && (
                        <span> - </span>
                      )}
                    </td>
                    <td>
                      {/*   {this.state.showEditPax === true && (
                        <input type="text" name="baggegeCode" onChange={this.handleChange} />
                      )} */}
                      {this.state.showEditPax === false && (
                        <span> - </span>
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>

              <div class="airline">
                <span>Airline : {this.state.showEditPax === true && (<select name="paxInfo.meal_request_code" value={paxInfo.meal_request_code} onChange={this.handleChange}>
                      {airlines}
                    </select>)}
                    {this.state.showEditPax === false && (
                      <span>{paxInfo.meal_request_code}</span>
                    )}</span>
                <span>Frequent flier number : </span>
                <span>Applicable Airline  : </span>
              </div>

            </div>
          ))}
        </div>
        <button type="button" id="editPax" hidden={this.state.showEditPax} onClick={this.handleEditPaxDetail} ><b>Edit Pax Details</b></button>
        <button type="button" id="savePax" hidden={!this.state.showEditPax} onClick={this.savePaxDetails} ><b>Save Pax Details</b></button>
        <button type="button" id="cancel" hidden={!this.state.showEditPax} onClick={this.handleEditPaxDetail} ><b>Cancel</b></button>
      </div>
    )
  }

}

export default FPHPax;


