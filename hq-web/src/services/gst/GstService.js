import axios from 'axios'
class GstService {
    
    fetchGstCompanyDetails(gstSearchReq) {
        console.log('fetchGstCompanyDetails Req '+JSON.stringify(gstSearchReq))
        return axios.post('http://192.168.44.141:9098/getCompanyGstDtls', gstSearchReq);
    }

     addGstCompanyDetails(addGstReq) {
        console.log('addGstCompanyDetails Req '+JSON.stringify(addGstReq))
        return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', addGstReq);
    }

    editGstCompanyDetails(editReq) {
        console.log('editGstCompanyDetails Req '+JSON.stringify(editReq))
        return axios.post('http://192.168.44.141:9098/updateCmpGstDetails', editReq);
    }

    deleteGstCompanyDetails(deleteReq) {
        console.log('deleteGstCompanyDetails Req '+JSON.stringify(deleteReq))
        return axios.post('http://192.168.44.141:9098/cmpGstDelete', deleteReq);
    }

}

export default new GstService()