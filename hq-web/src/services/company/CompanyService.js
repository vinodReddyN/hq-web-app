import axios from 'axios'
class CompanyService {
    
    fetchCompanyDetails(searchRequest) {
        console.log("Domain name search request : "+JSON.stringify(searchRequest));
        return axios.post('http://192.168.50.211:9098/getCmpConfByDomain',searchRequest);
    }

    addCompanyDetails(addReq) {
        return axios.post('http://192.168.50.211:9098/addEmploye', addReq);
    }

    editCompanyDetails(editReq) {
        return axios.post('http://192.168.50.211:9098/updateCompConf', editReq);
    }

    editCompanyContactDetails(editReq) {
        return axios.post('http://192.168.50.211:9098/updateCmpContDet', editReq);
    }

}

export default new CompanyService()