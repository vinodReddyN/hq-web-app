import React, { Component } from "react";
import GstService from "../../../services/gst/GstService.js";
import Icon from '../../../component/Icon';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import loader from '../../../assets/images/loader.gif';

class SearchGstCompanyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      companyName: "",
      companyGstDetails: "",
      isResult: false,
      pageData: "gstDetails",
      errorMsg: "",
      successMsg: this.props.successMsg,
      loading:''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // alert('jsonData'+JSON.stringify(jsonData));
    //alert('jsonData'+this.props.successMsg);
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value, successMsg: "" });
  };

  handleSubmit(e) {
    this.state.errorMsg = "";
    this.state.loading='true';

    if (
      this.state.companyName.trim() !== undefined &&
      this.state.companyName.trim() !== ""
    ) {
        this.searchGstComReq = {
            domain_name: this.state.companyName.trim()
          };

      this.searchCompanyReq(this.searchGstComReq);
      this.forceUpdate();
    } else {
    
      this.state.loading='false';
      this.forceUpdate();
      this.setState({ errorMsg: "Domain name must be provided" });
    }
  }

  searchCompanyReq(searchComReq) {
    this.state.errorMsg = "";
    GstService.fetchGstCompanyDetails(searchComReq).then(response => {
      const companyGstDetails = response.data;

      this.setState({ companyGstDetails });
      console.log(
        "fetchGstCompanyDetails Res " +
          JSON.stringify(this.state.companyGstDetails)
      );

      if (
        this.state.companyGstDetails !== undefined &&
        this.state.companyGstDetails !== "" &&
        this.state.companyGstDetails.responsecode !== undefined &&
        this.state.companyGstDetails.responsecode === 200
      ) {
        this.state.errorMsg = "";

        this.props.gstCompanyName(this.state.companyName);
        this.props.gstSearchHandler(this.state.companyGstDetails);
      } else {
        this.state.loading='false';
        this.forceUpdate();
        this.state.errorMsg = "Searched domain does not exist";
      }
    });
  }

  render() {
    return (
      <>
        <Header />
        <div className="MainContainer pageTtl">
          <h3>Search for the Company you want to edit</h3>
        </div>
        <div className="MainContainer cont-Box">
          {this.state.errorMsg !== "" && this.state.errorMsg !== undefined && (
            <div className="alert alert-warning">{this.state.errorMsg}</div>
          )}
          {this.state.successMsg !== "" &&
            this.state.successMsg !== undefined && (
              <div className="alert notification-success">
                {this.state.successMsg}
              </div>
            )}
          <div className="form-group">
            <div className="row">
              <div className="col-">
                <label>Domain name <span className="required">*</span> :</label>
                
              </div>
              <div className="col-2">
                <input placeholder="Enter your domain"
                  type="text"
                  name="companyName"
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-3">
                <button
                  type="button"
                  onClick={this.handleSubmit}
                  className="btn btn-primary btn-xs"
                >
                  <b>Search</b>
                </button>
                <span className="ldrPart">{this.state.loading==='true' && <img src={loader} className="App-logo" alt="loader" />}</span>
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </>
    );
  }
}
export default SearchGstCompanyComponent;
