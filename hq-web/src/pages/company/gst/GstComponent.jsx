import React, { Component } from "react";
import SearchGstCompanyComponent from "../gst/SearchGstCompanyComponent";
import AddGstDetailsComponent from "../gst/AddGstDetailsComponent";
import GstDetailsComponent from "../gst/GstDetailsComponent";

class GstComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyGstSearchPage: "true",
      addGstPage: "false",
      gstDetailsPage: "false",
      pageName: "search",
      gstResultsData: "",
      gstCompanyName: "",
      message: ""
    };

    this.searchGstrersult = this.searchGstrersult.bind(this);
    this.gstDetails = this.gstDetails.bind(this);
    this.companyNameEvent = this.companyNameEvent.bind(this);
    this.messageHandler = this.messageHandler.bind(this);
  }

  searchGstrersult(gstResults) {
    if (
      gstResults !== undefined &&
      gstResults.responsecode !== undefined &&
      gstResults.responsecode === 200
    ) {
      this.state.gstResultsData = gstResults;

      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "false",
        gstDetailsPage: "true"
      });
    }
  }
  companyNameEvent(gstSearchCompanyName) {
    this.state.gstCompanyName = gstSearchCompanyName;
  }

  gstDetails(gstData) {
    if (gstData !== "SEARCH PAGE") {
      this.setState({
        companyGstSearchPage: "false",
        addGstPage: "true",
        gstDetailsPage: "false",
        message: ''
      });
    } else if (gstData === "SEARCH PAGE") {
      this.setState({
        companyGstSearchPage: "true",
        addGstPage: "false",
        gstDetailsPage: "false",
        message: ''
      });
    }
  }

  messageHandler(msg) {
    if (msg !== undefined && msg !== "") {
      this.setState({
        companyGstSearchPage: "true",
        addGstPage: "false",
        gstDetailsPage: "false",
        message: msg
      });
    }
  }

  render() {
    var content;
    if (this.state.companyGstSearchPage === "true") {
      content = (
        <SearchGstCompanyComponent
          gstSearchHandler={this.searchGstrersult}
          gstCompanyName={this.companyNameEvent}
          successMsg={this.state.message}
        />
      );
    } else if (this.state.gstDetailsPage === "true") {
      content = (
        <GstDetailsComponent
          gstData={this.state.gstResultsData}
          gstCompanyName={this.state.gstCompanyName}
          gstDetailsHandler={this.gstDetails}
          gstDetailsMsg={this.messageHandler}
        />
      );
    } else if (this.state.addGstPage === "true") {
      content = (
        <AddGstDetailsComponent
          gstAddDetailsHandler={this.gstDetails}
          gstCompanyName={this.state.gstCompanyName}
          gstDetailsMsg={this.messageHandler}
        />
      );
    }

    return (
      <>
        <div>{content}</div>
      </>
    );
  }
}

export default GstComponent;
