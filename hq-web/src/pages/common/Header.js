import React, { Component } from 'react';
import MenuComponent from './MenuComponent';
import { Link } from 'react-router-dom'
import logo from '../../assets/images/logo.gif';
class Header extends Component {
  render() {
    return (
      <div className="App-header">
        <div className="MainContainer">
          <div className="Header">
          <Link to="/"><img src={logo} className="App-logo" alt="logo" /></Link>
            <MenuComponent />
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
