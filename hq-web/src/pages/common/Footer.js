import React, { Component } from 'react';

class Footer extends Component {
	
	constructor(){
		super();
		this.redirect=this.redirect.bind(this);
  	}
  
  	redirect(event){
		event.preventDefault();
		if(event.target.id === 'print'){
			window.location = "https://qa2.cleartrip.com/ticket"
		}else if(event.target.id === 'airFareMonth'){
			window.location = "https://qa2.cleartrip.com/calendar/"
		}else if(event.target.id === 'airFareSms'){
			window.location = "https://qa2.cleartrip.com/m"
		}else if(event.target.id === 'hotelDir'){
			window.location = "https://www.cleartrip.com/hotels/india/"
		}else if(event.target.id === 'blog'){
			window.location = "https://blog.cleartrip.com/"
		}else if(event.target.id === 'about'){
			window.location = "https://qa2.cleartrip.com/about/"
		}else if(event.target.id === 'faq'){
			window.location = "https://qa2.cleartrip.com/faq/"
		}else if(event.target.id === 'policy'){
			window.location = "https://qa2.cleartrip.com/privacy/"
		}else if(event.target.id === 'security'){
			window.location = "https://qa2.cleartrip.com/security/"
		}else if(event.target.id === 'terms'){
			window.location = "https://qa2.cleartrip.com/terms/"
		}else if(event.target.id === 'contact'){
			window.location = "https://qa2.cleartrip.com/contact/"
		}
  	}

  render() {
    return (
      <div className="Footer">      
      <div className="MainContainer">
    	<ul id="">
         	<li className="first"><a id="print" href="/ticket" onClick={this.redirect} title="Print or email e-tickets booked with Cleartrip">Print/Email your e-tickets</a></li>
	    	<li><a id="airFareMonth" href="/calendar" onClick={this.redirect} title="See the best air fares for an entire month">See best fares by month</a></li>
    		<li><a id="airFareSms" href="/mobile" onClick={this.redirect} title="Get airfares at your fingertips via SMS">Search airfares via SMS</a></li>
    		<li><a id="hotelDir" href="/hotels/india/" onClick={this.redirect} title="India hotel directory">India hotel directory</a></li>
    	</ul>
    	<ul className="footerSub">
    		<li className="first"><a id="blog" href="/blog/" onClick={this.redirect}>Cleartrip Blog</a></li>
    		<li><a id="about" href="/about/" onClick={this.redirect}>About Cleartrip</a></li>
    		<li><a id="faq" href="/faq/" onClick={this.redirect}>FAQs</a></li>
    		<li><a id="policy" href="/privacy/" onClick={this.redirect}>Privacy Policy</a></li>
    		<li><a id="security" href="/security/" onClick={this.redirect}>Security</a></li>
    		<li><a id="terms" href="/terms/" onClick={this.redirect}>Terms of use</a></li>
    		<li><a id="contact" href="/contact/" onClick={this.redirect}>Contact</a></li>
    	</ul>
    	<p>© 2006–2019 Cleartrip Travel Services Private Limited</p>
    </div>
      </div>
    );
  }
}

export default Footer;