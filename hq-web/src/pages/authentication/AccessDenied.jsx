import { Route, Redirect } from 'react-router-dom'
import React, { Component } from 'react'

class AccessDenied extends Component{
    render() {
        return (
            <>
                <h2>Access Denied..</h2>
                <div className="container">
                    Please contact with Administrator....
                </div>
            </>
        )
    }
}

export default AccessDenied

