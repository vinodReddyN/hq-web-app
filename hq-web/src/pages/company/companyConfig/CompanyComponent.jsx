import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import AddCompanyComponent from './AddCompanyComponent';
import EditCompanyComponent from './EditCompanyComponent';
import SearchCompanyComponent from './SearchCompanyComponent';
import CompanyDetailsComponent from './CompanyDetailsComponent';
import CheckCompanyComponent from './CheckCompanyComponent';
class CompanyComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchPage: 'true',
            detailsPage: "false",
            checkAddPage: 'false',
            addPage: 'false',
            editPage: 'false',
            detailsPagedata: {},
            domainName: '',
            responseDataUI: {}
        }
        this.getSearchedData = this.getSearchedData.bind(this);
        this.editPage = this.editPage.bind(this);
        this.addPage = this.addPage.bind(this);
        this.checkAddPage = this.checkAddPage.bind(this);
        this.editBackBtn = this.editBackBtn.bind(this);
        this.setDomainName = this.setDomainName.bind(this);
    }

    getSearchedData(data) {
        if (data !== undefined && data.status !== undefined && data.status !== '' && data.status == 200) {
            var filteredData = {
                companyId: data.company.id,
                domain_name: data.company.domain_name,
                name: data.company.name,
                city: data.company.city,

                emailId: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.emails !== undefined
                    && data.company.contact_data.emails.length > 0) ? data.company.contact_data.emails[0].email_id : '',

                emailType: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.emails !== undefined
                    && data.company.contact_data.emails.length > 0) ? data.company.contact_data.emails[0].category : '',

                contactNum: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.phone_numbers !== undefined
                    && data.company.contact_data.phone_numbers.length > 0) ? data.company.contact_data.phone_numbers[0].phone_number_value : '',

                contactType: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.phone_numbers !== undefined
                    && data.company.contact_data.phone_numbers.length > 0) ? data.company.contact_data.phone_numbers[0].category : '',

                address: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.addresses !== undefined
                    && data.company.contact_data.addresses.length > 0) ? data.company.contact_data.addresses[0].street_address : '',

                addressType: (data.company !== undefined && data.company.contact_data !== undefined
                    && data.company.contact_data.addresses !== undefined
                    && data.company.contact_data.addresses.length > 0) ? data.company.contact_data.addresses[0].category : '',

                contact_data: (data.company.contact_data !== undefined) ? data.company.contact_data : {},
                company_configs: (data.company.company_configs !== undefined && data.company.company_configs.length > 0) ? data.company.company_configs : {}
            }
            this.setState({ detailsPagedata: filteredData, detailsPage: 'true', searchPage: 'false', checkAddPage: 'false', addPage: 'false', editPage: 'false' });
        } else {
            this.setState({ searchPage: 'true', checkAddPage: 'false', detailsPage: 'false', addPage: 'false', editPage: 'false' });
        }
    }

    editPage(cmpDetailsInfo) {
        this.setState({ editPage: 'true', searchPage: 'false', checkAddPage: 'false', detailsPage: 'false', addPage: 'false' });
    }

    editBackBtn(data) {
        this.setState({ searchPage: 'true', checkAddPage: 'false', detailsPage: 'false', editPage: 'false', addPage: 'false' });
    }

    addPage(data) {
        if (data === 'addPage') {
            this.setState({ addPage: 'true', checkAddPage: 'false', detailsPage: 'false', searchPage: 'false', editPage: 'false' });
        }
    }

    checkAddPage(data) {
        if (data === 'checkAddPage') {
            this.setState({ checkAddPage: 'true', addPage: 'false', detailsPage: 'false', searchPage: 'false', editPage: 'false' });
        }
    }

    setDomainName(name) {
        this.setState({ domainName: name });
    }


    render() {
        var content;
        if (this.state.searchPage === 'true') {
            content = <SearchCompanyComponent clickSearchHandler={this.getSearchedData} getDomainName={this.setDomainName}/>
        } else if (this.state.checkAddPage === 'true') {
            content = <CheckCompanyComponent clickCheckAddHandler={this.addPage} dname={this.state.domainName}/>
        } else if (this.state.addPage === 'true') {
            content = <AddCompanyComponent clickAddHandler={this.checkAddPage} dname={this.state.domainName} />
        } else if (this.state.editPage === 'true') {
            content = <EditCompanyComponent detailsPagedata={this.state.detailsPagedata} clickEditBackAddHandler={this.editBackBtn} />
        } else if (this.state.detailsPage === 'true') {
            content = <CompanyDetailsComponent detailsPagedata={this.state.detailsPagedata} clickCompanyDetailsHandler={this.editPage} clickEditBackAddHandler={this.editBackBtn}/>
        }

        return (
            <>
                <div>
                    {content}
                </div>
            </>
        )
    }
}

export default CompanyComponent
