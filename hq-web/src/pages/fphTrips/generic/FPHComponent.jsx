import React, { Component } from 'react'
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import FPHTrip from '../trips/FPHTrip'
import FPHPax from '../pax/FPHPax'
import FPHNotes from '../notes/FPHNotes'
import FPHPayment from '../payment/FPHPayment'
import FPHAuditTrail from '../audit/FPHAuditTrail'
import FPHUpdateTktNumber from '../updateTicket/FPHUpdateTktNumber'
import FPHUpdatePricing from '../updatePrice/FPHUpdatePricing'
import FPHRefunds from '../refund/FPHRefunds'
import { Link } from 'react-router-dom'

let fphRS = require('../../../assets/json/data.json');
 

class FPHComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tripid: this.props.match.params.tripId,
            content: '',
            isActive: false,
            fltDtls:[],
            tripDtl:fphRS
        }
        this.renderComponents = this.renderComponents.bind(this);
        console.log('param:'+this.state.tripid)
        this.loadData(this.state.tripid);

    }

    loadData(tripId){
        console.log('load data:'+tripId)
    }

    renderComponents(e) {
        this.currentClick = e.target.id
        if(e.target.id === 'tripDetails'){
            
           // tripObj=this.state.tripDtl.air_bookings;
            let tripObj = Object.create(this.state.tripDtl);
            let airBkng= tripObj.air_bookings;
            let fltBookDtl = airBkng[0].flights;
            var segArray=[];
            var segArrayReturn=[];
         
            let fltDtlList =[];
            
     for(var fltDtl of this.state.tripDtl.air_bookings[0].flights){
       fltDtl.type_detail = "Flight";
      fltDtlList.push(fltDtl);
     }
     var seg;
     for(seg of fltBookDtl[0].segments){
       segArray.push(seg);
     }
     for(seg of fltBookDtl[1].segments){
       segArrayReturn.push(seg);
     }
     for(seg of segArrayReturn){
       segArray.push(seg);
     }    
     let airBkngInfo = airBkng[0].air_booking_infos;
     var bkng;
     for(bkng of airBkngInfo){
       for(seg of segArray){
         if(bkng.segment_id===seg.id){
           bkng.departure_airport=seg.departure_airport;
           bkng.arrival_airport=seg.arrival_airport;
           break;
         }
       }
     }
     for(bkng of airBkngInfo){
       bkng.pricing_objects=[];
      for(var priceObj of airBkng[0].pricing_objects){
           if(bkng.pricing_object_id=== priceObj.id){
            bkng.pricing_objects.push(priceObj);
           }
      }
     }
      
     let paxToFlightMap = airBkng[0].pax_infos;
     
     for(var pax of paxToFlightMap){
       pax.type_detail = "Pax";
       pax.paxToFlightMap=[];
       for(var bkng  of airBkngInfo){
         if(pax.id===bkng.pax_info_id){
          pax.paxToFlightMap.push(bkng)
         }
       }
     }
     for(var paxInfo of paxToFlightMap){
      fltDtlList.push(paxInfo);
     }

     let htlDtlList =[];
     let htlBookObj = new Object();
     let htlBkngDtl = tripObj.hotel_bookings[0];
     htlBookObj.check_in_date = htlBkngDtl.check_in_date;
     htlBookObj.check_out_date = htlBkngDtl.check_out_date;
     htlBookObj.city_name = htlBkngDtl.hotel_detail.full_city_name;
     htlBookObj.hotel_name =htlBkngDtl.hotel_detail.name;
     htlBookObj.room_type=htlBkngDtl.room_types[0].name;
     htlBookObj.room_count=htlBkngDtl.room_count;
     htlBookObj.type="hotel_booking";
     htlDtlList.push(htlBookObj);
     
     
     for(let type of htlBkngDtl.room_types){
     let roomDtlObj = new Object();
     roomDtlObj.room_info =[];
     roomDtlObj.type="room details";
     roomDtlObj.room_count =htlBkngDtl.room_count;
     roomDtlObj.no_of_adult=htlBkngDtl.hotel_booking_infos.length;
     roomDtlObj.inclusion=type.inclusions;
        for(let room of htlBkngDtl.rooms){
            if(type.id===room.room_type_id){
             let roomObj = new Object();
             
            for(let info of htlBkngDtl.hotel_booking_infos){
                if(info.room_id===room.id){
                   roomObj.room_name=type.name;
                   roomObj.supplier_name=type.supplier_id;
                   roomObj.voucher_number =info.voucher_number;
                   roomObj.status=info.booking_status;
                   roomDtlObj.room_info.push(roomObj);
                   break;
                }
            }
           }
        }
        htlDtlList.push(roomDtlObj);
    }     
      
     console.log(htlDtlList);   
            this.state.content =<FPHTrip fltDtl={fltDtlList} htlDtl={htlDtlList}/>;
        }else if (e.target.id === 'pax') {
            this.state.content =<FPHPax/>;
        } else if (e.target.id === 'notes') {
            this.state.content =<FPHNotes {...this.state}/>;
        }else if (e.target.id === 'paymentDetails') {
           this.state.content =<FPHPayment/>;
        }else if (e.target.id === 'audittrail') {
           this.state.content =<FPHAuditTrail/>;
        }else if (e.target.id === 'updateTktNum') {
           this.state.content =<FPHUpdateTktNumber/>;
        }else if (e.target.id === 'updatePricing') {
          this.state.content =<FPHUpdatePricing/>;
        }else if (e.target.id === 'refunds') {
           this.state.content =<FPHRefunds/>;
          }
        

    }

    

    render() {

        let { tripid } = this.state
        return (

            <>
                <Header />
                <div className="MainContainer cont-Box fphPage">
                    <div className="row">
                        <div className="col-9">
                            <div className="tripInfo">
                                <h5>{this.state.tripDtl.trip_name} (Trip ID: {this.state.tripDtl.trip_ref})</h5>
                                <span className="bookingdate">Booked by <a href="#">{this.state.tripDtl.booked_user_id}</a> {this.state.tripDtl.air_bookings[0].created_at}</span>
                                <span className="userType">Overall : <span className="Newcust">L1</span>  <span className="Newcust"> New User</span> Product (Package) : <span className="Newcust">New User</span> </span>
                                </div>
                            <ul className="triplink">
                                <li>
                                    <Link id="tripDetails" onClick={this.renderComponents} className={this.currentClick==="tripDetails"? "active":""}>Trip Details</Link>
                                </li>
                                <li>
                                    <Link id="pax" onClick={this.renderComponents} className={this.currentClick==="pax"? "active":""}>Pax Details</Link>
                                </li>
                                <li>
                                    <Link id="notes" onClick={this.renderComponents} className={this.currentClick==="notes"? "active":""}>Notes</Link>
                                </li>
                                <li>
                                    <Link id="paymentDetails" onClick={this.renderComponents} className={this.currentClick==="paymentDetails"? "active":""}>Payment Details</Link>
                                </li>
                                <li>
                                    <Link id="audittrail" onClick={this.renderComponents} className={this.currentClick==="audittrail"? "active":""}>Audi Trail</Link>
                                </li>
                                <li>
                                    <Link id="updateTktNum" onClick={this.renderComponents} className={this.currentClick==="updateTktNum"? "active":""}>Update Ticket Number</Link>
                                </li>
                                <li>
                                    <Link id="updatePricing" onClick={this.renderComponents} className={this.currentClick==="updatePricing"? "active":""}>Update Pricing</Link>
                                </li>
                                <li>
                                    <Link id="refunds" onClick={this.renderComponents} className={this.currentClick==="refunds"? "active":""}>Refunds</Link>
                                </li>
                            </ul>
                            
                            <div className="tripDetails">
                                {this.state.content}
                            </div>
                        </div>
                        <div className="col-3">
                        <Link>Email Trip Detail</Link>
                        <Link>SMS Trip Detail</Link>
                        <Link>Air Cancel</Link>
                        <Link>Hotel Cancel</Link>
                        </div>
                        
                    </div>
                </div>
                <Footer />

            </>

        )
    }
}
export default FPHComponent;