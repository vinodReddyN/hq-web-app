import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import EmployeeService from '../../services/people/EmployeeService';
import Calendar from 'ciqu-react-calendar';
import ReactAutocomplete from 'react-autocomplete';
import moment from 'moment';

class EditPeopleComponent extends React.Component{
    constructor(props){
    super(props)
    this.state={
    title: '',
    empFirstName: '',
    empLastName:'',
    empEmailId:'',
    contactNum:'',
    value:'',
    gender:'Male',
    startDate: moment(),
    comment:''}
    this.handleSubmit=this.handleSubmit.bind(this);
    }
    onOpenChange = (status) => {
        console.log('open status: ' + status)
      }
      handleChange= (e)=> {
        this.setState({[e.target.name]:e.target.value});
        }
    
    handleSubmit(e) {
      console.log("start handleSubmit");
      this.createReq = {
        title:this.state.title,
        empFirstName: this.state.empFirstName,
        empLastName: this.state.empLastName,
        empEmailId: this.state.empEmailId,
        contactNum:this.state.contactNum,
        value:this.state.value,
        gender:this.state.gender,
        startDate:this.state.startDate,
        comment:this.state.comment
      }
      alert(JSON.stringify(this.createReq));
      this.addEmpPeople(this.createReq);
      
  }
  
  addEmpPeople(createReq) {
    EmployeeService.addPeople(createReq)
        .then(response => {
            const persons = response.data;
            this.setState({ persons });
        })
        .catch(this.setState({ persons: '' }))
  }
  
  onChange = (startDate, inputValue) => {
    console.log(startDate.format('DD-MM-YYYY'))
    this.setState({startDate})
  }


    render(){
        const {onChange, onOpenChange, disabledDate} = this.state
        return (
            <div>
               <h1>Edit Peoples details..</h1>
               <h2> Lakshmi narayana <Link  to={'/'}>Edit </Link></h2>
               <h3> Cleartrip </h3>

        <Tabs>
          <TabList>
            <Tab>Contact Info</Tab> &emsp;
            <Tab>Travel preference</Tab> &emsp;
            <Tab>Photo</Tab> &emsp;
            <Tab>Account</Tab> &emsp;
            <Tab>Company details</Tab>
          </TabList>
    <TabPanel>
    <div>
      <div className="row justify-content-md-center">
      <div className="col-md-6 col-md-offset-3">
      <div className="form-group">
      <br />
      <label>Title:</label>
      <select  name="title" value={this.state.title} onChange={this.handleChange}>
            <option value="Mr">Mr</option>
            <option value="Miss">Miss</option>
            <option value="Mstr">Mstr</option>
      </select>
      <br/>
      <label>Gender: </label>
      <input  type="radio" name="gender" label="Male" onChange={this.handleChange}  value="Male"  />Male 
      <input type="radio" name="gender" label="Femele" onChange={this.handleChange}  value="Female"  />Femele
      <br/>
      <label>First name:</label>
      <input  type="text"  name="empFirstName" onChange={this.handleChange} value={this.state.empFirstName}></input><br/>
      <label>Last name:</label>
      <input  type="text"  name="empLastName" onChange={this.handleChange} value={this.state.empLastName}></input>
      <br/>
      <label>Email Id:</label>
      <input  type="text"  name="empEmailId" onChange={this.handleChange} value={this.state.empEmailId}></input>
      <br/>
      <label>Contact Num:</label>
      <input  type="text"  name="contactNum" onChange={this.handleChange} value={this.state.contactNum}></input>
      <br/>
      <label>Nationality:</label>
      <ReactAutocomplete
        items={[
          { id: '1', label: 'India' },
          { id: '2', label: 'Japan' },
          { id: '3', label: 'United Kingdom' },
          { id: '4', label: 'Saudi Arebia' },
        ]}
        shouldItemRender={(item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
        getItemValue={item => item.label}
        renderItem={(item, highlighted) =>
          <div
            key={item.id}
            style={{ backgroundColor: highlighted ? '#eee' : 'transparent'}}
          >
            {item.label}
          </div>
        }
        value={this.state.value}
        onChange={e => this.setState({ value: e.target.value })}
        onSelect={value => this.setState({ value })}
      />
      <br/>
      <label>Date of Birth:</label>
      <Calendar
        onChange={onChange} 
        value={this.state.startDate}
        allowClear={true} 
        disabled={false}
        placeholder={'Please select date'}
        format={'DD-MM-YYYY'}
        onOpenChange={onOpenChange}
        disabledDate={disabledDate}
      />
      <br/>
      <br/>
      
      <label>Comments</label>
      <textarea name="comment" value={this.state.comment} onChange={this.handleChange} cols={30} rows={3} />
     </div>
      </div>
      </div>
      <button type="button" onClick={this.handleSubmit} className="btn btn-primary"><b>Add Employe</b></button>
      </div>
  </TabPanel>
    <TabPanel>
      <h2>Travel preference here..</h2>
    </TabPanel>
    <TabPanel>
      <h2>Photo here..</h2>
    </TabPanel>
    <TabPanel>
      <h2>Account here.. </h2>
    </TabPanel>
    <TabPanel>
      <h2>Company details here..</h2>
    </TabPanel>
 </Tabs>
        </div>
            
         
        )
     }
}
export default EditPeopleComponent