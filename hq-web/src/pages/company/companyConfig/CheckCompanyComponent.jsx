import React, { Component } from 'react'
import ReactAutocomplete from 'react-autocomplete';
import moment from 'moment';
import Header from '../../../pages/common/Header'
import Footer from '../../../pages/common/Footer'
import Icon from '../../../component/Icon';
class CheckCompanyComponent extends React.Component {
    constructor(props) {
        super(props)
        var dmn = this.props.dname
        this.state = {
            domainName: dmn,
            errorMsg: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleSubmit(e) {
        this.setState({ errorMsg: '' })
        if (this.state.domainName === undefined || this.state.domainName.trim() === '') {
            this.setState({ errorMsg: 'Domain name must be provided' })
        } else {
            this.createReq = {
                domainName: this.state.domainName.trim(),
            }
            this.props.clickCheckAddHandler('addPage');
        }
    }

    render() {
        const { channelName } = this.state
        return (
            <>
                <Header />
                <div className="MainContainer pageTtl">
                <h3>Company Config Details</h3>
                </div>
                <div className="MainContainer cont-Box">
                    {this.state.errorMsg !== '' && <div className="alert alert-warning">{this.state.errorMsg}</div>}
                    
                    <p>This subdomain is not on the list. Do you want to add it?</p>
                    <div className="form-group">
                        <div class="row">
                            <div className="col-5">
                                <div class="row">
                                    <div className="col-4"><label>Domain Name:</label></div>
                                    <div className="col-6"><input type="text" name="domainName" value={this.state.domainName} onChange={this.handleChange} /></div>
                                    <div className="col-1"><button type="button" onClick={this.handleSubmit} className="btn btn-xs btn-primary"><b>Add</b></button></div>
                                </div>
                            </div>
                        </div>
                    </div>

                   
                </div>
                <Footer />
            </>
        )
    }
}
export default CheckCompanyComponent